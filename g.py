#File for all our globals
import logging

class Globals():
    log = None

    def get_logger_module(self):
        return logging

    def set_log(self, l):
        '''Set the global log for our app'''
        self.log = l
        
    def get_log(self):
        return self.log


#Global object
g = Globals()