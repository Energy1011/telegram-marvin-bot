FROM python:3.7

WORKDIR /bot

RUN apt update

COPY requirements.txt .

RUN pip install -r requirements.txt

CMD ["python3", "/bot/marvinbot.py"]