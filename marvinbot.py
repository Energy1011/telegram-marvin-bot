#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author energy1011
# inspired and fork from: git@github.com:atareao/senderbot.git

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import telegram
import argparse

from telegram import Bot
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import CallbackQueryHandler, ConversationHandler
from telegram import InlineQueryResultArticle, InputTextMessageContent
from telegram.ext import InlineQueryHandler
from warnings import filterwarnings

from time import sleep
import os
import codecs
import json
from functools import wraps
import importlib


import lxml.html as lh
from lxml.html.clean import clean_html
from uuid import uuid4
import requests


from g import g

update_id = None

APP = 'MarvinBot'
APPCONF = APP.lower() + '.conf'
CONFIG_DIR = os.path.join(os.path.expanduser('~'), '.config')

# CONFIG_APP_DIR = os.path.join(CONFIG_DIR, APP.lower())
# CONFIG_FILE = os.path.join(CONFIG_APP_DIR, APPCONF)

CONFIG_APP_DIR = os.path.dirname(__file__)
CONFIG_FILE = APPCONF

PARAMS = {
    'token': None,
    'channel_id': None,	
    'ssh_server': None,
	'ssh_user': None,
	'ssh_key_file_password': None,
	'ssh_keyfile_path': None,
	'ssh_port': 22,
	'marvin_end_point_dir': None,
	'team_gorup': None
}

REPO_URL = 'https://gitlab.com/Energy1011/telegram-marvin-bot'

class Configuration(object):
    """Class to manage conf files"""
    def __init__(self):
        self.params = PARAMS
        self.read()

    def get(self, key):
        try:
            return self.params[key]
        except KeyError as e:
            self.params[key] = PARAMS[key]
            return self.params[key]

    def set(self, key, value):
        self.params[key] = value

    def reset(self):
        if os.path.exists(CONFIG_FILE):
            os.remove(CONFIG_FILE)
        self.params = PARAMS
        self.save()

    def set_defaults(self):
        self.params = PARAMS
        self.save()

    def print_config(self):
        log.info(self.params)

    def save(self):
        # TODO: create this file copying example
        f = open(CONFIG_FILE, 'w+')
        f.write(json.dumps(PARAMS).replace("\'", "\""))
        f.close()

    def read(self):
        try:
            f = codecs.open(CONFIG_FILE, 'r', 'utf-8')
        except IOError as e:
            log.error(e)
            #create conf file and dir if not exists
            if not os.path.exists(CONFIG_APP_DIR):
                os.makedirs(CONFIG_APP_DIR)
            self.save()
            f = codecs.open(CONFIG_FILE, 'r', 'utf-8')

        try:
            self.params = json.loads(f.read())
        except ValueError as e:
            log.critical("Error reading conf file, please remove single quotes ' and replace by doubles. You can copy marvinbot.conf.example an rename into marvinbot config file")
            exit(1)
            # self.save()
        f.close()

class MarvinBot(Bot):

    lastQueryHandler = None
    CANCEL_CONV = -1
    SELECT_SSHHOST_ACTION, SELECT_SSHHOST_ACTION_EXEC = range(2)

    def __init__(self, token=None, channel_id=None):
        if token is None or channel_id is None:
            self.configuration = Configuration()
            self.token = self.configuration.get('token')
            self.channel_id = self.configuration.get('channel_id')
        if self.token is None or len(self.token) < 20 or self.channel_id is None:
            log.critical("Please set valid token and channel_id in config file")
            exit(1)

        Bot.__init__(self, self.token)
            
        if self.token is not None and self.channel_id is not None:
            self.start_service()
        else:
            log.critical("Error set token and channel_id passing args or setting in config file, for more information: " + REPO_URL)
            exit(1)
            
    def _private_function_decorator(func):
        """ This method is used as middleware in all bot methods control access to public or private bot methods"""
        @wraps(func)
        def middleware(inst, update, context):
            allow_user = False
            for user in inst.configuration.get('team_group'):
                if str(update.effective_chat.id) == str(inst.configuration.get('team_group')[user]['chat_id']):
                    log.info('User accepted "%s"', update)
                    allow_user = True
                    break
            if not allow_user:
                log.warning('No Allow User "%s"', update)
                msg = "Esta es una acción no permitida, te lo dije, todo siempre sale mal."
                inst.send_message(update, context, msg)
                return None
            result = func(inst, update, context)
            return result 
        return middleware

    def start_service(self):
        """Run the bot as service background mode."""
        self.updater = Updater(token=self.token, use_context=True)
        self.dispatcher = self.updater.dispatcher
        # self.bot = telegram.Bot(token=self.token)
        log.info(APP + " Running...")

        handler = CommandHandler('start', self.start)
        self.dispatcher.add_handler(handler)
        
        handler = CommandHandler('bkdb', self.bkdb)
        self.dispatcher.add_handler(handler)

        handler = CommandHandler('serverctl', self.serverctl)
        self.dispatcher.add_handler(handler)

        handler = CommandHandler('marvinreloadconf', self.marvinreloadconf)
        self.dispatcher.add_handler(handler)

        handler = CommandHandler('bkgsheet', self.bkgsheet)
        self.dispatcher.add_handler(handler)

        handler = CommandHandler('fail2banunban', self.fail2banunban)
        self.dispatcher.add_handler(handler)

        handler = CommandHandler('yesno', self.yesno)
        self.dispatcher.add_handler(handler)

        handler = CommandHandler('sendmechatid', self.sendme_chat_id)
        self.dispatcher.add_handler(handler)

        handler = CommandHandler('dhltracking', self.dhltracking)
        self.dispatcher.add_handler(handler)

        handler = CommandHandler('coingeckomonitor', self.coingeckomonitor)
        self.dispatcher.add_handler(handler)

        handler = CommandHandler('coingeckomonitorlist', self.coingeckomonitorlist)
        self.dispatcher.add_handler(handler)

        handler = CommandHandler('bitsomonitor', self.bitsomonitor)
        self.dispatcher.add_handler(handler)

        handler = CommandHandler('bitsomonitorlist', self.bitsomonitorlist)
        self.dispatcher.add_handler(handler)

        handler = CommandHandler('vinchecksum', self.vinchecksum)
        self.dispatcher.add_handler(handler)

        handler = CommandHandler('help', self.help)
        self.dispatcher.add_handler(handler)

        handler = InlineQueryHandler(self.inlinequery)
        self.dispatcher.add_handler(handler)

        # hide/ignore warnings
        filterwarnings(action="ignore", message=r".*CallbackQueryHandler")
        # SshHost conversation handler
        sshhost_conv_handler = ConversationHandler(
            per_user=True,
            per_chat=True,
            entry_points=[CommandHandler('sshhost', self.select_sshhost)],
            states={
                self.SELECT_SSHHOST_ACTION: [CallbackQueryHandler(self.select_sshhost_action)],
                self.SELECT_SSHHOST_ACTION_EXEC: [CallbackQueryHandler(self.select_sshhost_action_exec)],
            },
            fallbacks=[CallbackQueryHandler(self.select_sshhost),
            ],
        )

        self.dispatcher.add_handler(sshhost_conv_handler)

        self.dispatcher.add_error_handler(self.error)

        self.updater.start_polling(allowed_updates=[])
        self.updater.idle()
    
    def definition(self, word):
        url = 'http://www.wordreference.com/definicion/%s' % (word)
        response = requests.get(url)
        if response.status_code == 200:
            doc = lh.document_fromstring(clean_html(response.text))
            trans = doc.find_class('trans')
            if len(trans) > 0:
                trans_tmp = ''
                for t in trans:
                    trans_tmp = trans_tmp + lh.tostring(t,
                                                        method='text',
                                                        encoding='utf-8').decode()
                    trans_tmp = trans_tmp.replace('.  ', '.\n')
                    trans_tmp = trans_tmp.replace('. ', '.\n')
                return trans_tmp
        return None 
    
    def inlineWordreference(self, update, context, query):
        if not query:
            return
        results = list()
        definition = self.definition(query)
        if definition is not None:
            results.append(
                InlineQueryResultArticle(
                    id=uuid4(),
                    title='WordReference: '+query,
                    input_message_content=InputTextMessageContent(definition)
                )
                
            )
            update.inline_query.answer(results)
            return True
        return False

    def inlineJisti(self, update, context, query):
        results = list()
        query = "https://meet.jit.si/-{}-".format(query)
        results.append(
                InlineQueryResultArticle(
                    id=uuid4(),
                    title='Jitsi: '+query,
                    input_message_content=InputTextMessageContent(query)
                )
                
            )
        update.inline_query.answer(results)
        return True
            
    def inlinequery(self, update, context):
        """Handle the inline query."""
        query = update.inline_query.query
        cmd = query.split(" ")
        if cmd[0].lower() == 'wordreference':
            self.inlineWordreference(update, context, " ".join(cmd[1:]))
            return True
        if cmd[0].lower() == 'jitsichat':
            self.inlineJisti(
                            update, context, " ".join(cmd[1:]))
            return True
        return False
        
    def error(self, update, context):
        """Log Errors caused by Updates."""
        log.warning('Update "%s" caused error "%s"', update, context.error)

    # All bot functions
    def send_message(self, update, context, msg):
        context.bot.send_message(chat_id=update.effective_chat.id,text=msg)

    def sendme_chat_id(self, update, context):
        msg = '{} es el chat_id de nuestra triste y aburrida conversación'.format(update.effective_chat.id)
        context.bot.send_message(chat_id=update.effective_chat.id,text=msg)

    def start(self, update, context):
        msg = "Justo cuando pensaba que esto no podría ser peor... \n" + update.effective_chat.first_name + " con /help puede ver un listado de tareas inservibles que puedo realizar"
        self.send_message(update, context, msg)

    def help(self, update, context):
        msg = "/start :Inicia MarvinBot\n\n"
        for actionName in self.configuration.get('marvinSkills'):
            action = self.configuration.get('marvinSkills')[actionName]
            if 'long' in context.args:
                if action['desc']:
                    actionName = actionName + " :"+ action['desc']
            if action['private'] == 1:
                msg = msg + "/"+actionName+" (private)\n\n"
            else:
                msg = msg + "/"+actionName+"\n\n"
        self.send_message(update, context, msg)

    def show_options(self, update, context, commandName = None, msg = None, options = []):
        # each time show option we need to remove last query handler 
        self.finish_execute_query_handler()
        if len(options) <= 0:
            skill = self.configuration.get('marvinSkills')
            options = skill[commandName]['options'] or []
        keyboard =  []
        button = []
        for option in options:
            button.append(InlineKeyboardButton(option['name'], callback_data=option['callback_data']))
        button.append(InlineKeyboardButton('Cancelar', callback_data=self.CANCEL_CONV))
        keyboard.append(button) 
        reply_markup = InlineKeyboardMarkup(keyboard).from_column(button)
        update.message.reply_text(msg+'\nOpciones:', reply_markup=reply_markup)

    def cancel_option_query_handler(self, update, context):
        self.send_message(update, context, 'Has cancelado la acción, era inútil intentarlo de cualquier modo...')
        self.finish_execute_query_handler()

    def finish_execute_query_handler(self):
        #remove_handler callbackQueryHandler it means: 
            # once the button option is pressed you cant press it again and starts this execution again.
        self.dispatcher.remove_handler(self.lastQueryHandler)

    def get_user_keyboard_option_pressed(self, update):
        query = update.callback_query
        query.answer()
        return query.data

    @_private_function_decorator
    def bkdb(self, update, context):
        msg = "¿ Realizar un respaldo ? Realmente no hay nada divertido en el universo..."
        self.show_options(update, context, 'bkdb', msg)

        #Define or execute for this skill method 
        def execute(update, context):
            user_response = self.get_user_keyboard_option_pressed(update)
            if user_response == self.CANCEL_CONV:
                self.cancel_option_query_handler(update, context)
                return False
            self.finish_execute_query_handler()
            bkdb = importlib.import_module('marvinSkills.private.bkdb')
            result = bkdb.bkdb(self, update, context, user_response)
            if result['status'] :
                msg = "Ahí tienes tu respaldo, los humanos siempre terminan perdiendo o estropeando los respaldos."
                self.send_message(update, context, msg)
                return True
            else:
                self.send_message(update, context, result['msg'])
                return False

        # Save last and add CallbackQueryHandler for our execution
        self.lastQueryHandler = CallbackQueryHandler(execute)
        self.dispatcher.add_handler(self.lastQueryHandler)
    
    def get_update_user_name(self, update):
        return update.message.chat.username

    def get_update_username_chat_id(self, update):
        return update.message.chat.id

    def get_user_data_from_config_by_chat_id(self, chat_id):
        for user in self.configuration.get('team_group'):
            if str(chat_id) == str(self.configuration.get('team_group')[user]['chat_id']):
                return self.configuration.get('team_group')[user]
        return None

    def filter_options(self, needles, haystack):
        '''
            This filter is used to iter needles 'keys' to find in kaystack 'config section' 
            this filter can operate with needles as list using key_name attr
        '''
        options = []
        if type(needles) == list:
            for needle in needles:
                if needle['key_name'] in haystack or '*' in haystack:
                    options.append(needle)
        else:
            for needle in needles:
                if needle in haystack:
                    options.append({"name": needle.upper(), "callback_data":needle})
        return options
    
    def select_sshhost_action_exec(self, update, context):
        user_selected_action = update.callback_query.data
        if user_selected_action == str(self.CANCEL_CONV):
            # self.cancel_option_query_handler(update, context)
            return self.CANCEL_CONV
        skill = importlib.import_module('marvinSkills.private.sshhost') 
        host, action = user_selected_action.split('|')
        result = skill.sshhost(self, update, context, host, action)
        self.send_message(update, context, 'Ejecución de endpoint finalizada.')
        self.finish_execute_query_handler()
        return self.CANCEL_CONV
    
    def select_sshhost_action(self, update, context):
        query = update.callback_query

        user_selected_host = update['callback_query']['data']
        if user_selected_host == str(self.CANCEL_CONV):
            # self.cancel_option_query_handler(update, context)
            return self.CANCEL_CONV
        user_data = self.get_user_data_from_config_by_chat_id(query.message.chat.id)
        ssh_multi_host = self.configuration.get('ssh_multi_host') or []

        msg = "Sabes que nada hago bien ¿ Cómo quieres que lo eche a perder ?"
        allow_user_actions =  user_data['ssh_multi_host_access'][user_selected_host] or []
        host_selected_options = ssh_multi_host[user_selected_host]['options'] or []
        
        options = self.filter_options(host_selected_options, allow_user_actions)

        keyboard =  []
        button = []
        for option in options:
            button.append(InlineKeyboardButton(option['name'], callback_data=user_selected_host+'|'+option['callback_data']))
        button.append(InlineKeyboardButton('Cancelar', callback_data=self.CANCEL_CONV))
        keyboard.append(button) 
        reply_markup = InlineKeyboardMarkup(keyboard).from_column(button)
        
        self.edit_message_text(
            chat_id=query.message.chat_id,
            message_id=query.message.message_id,
            text=msg,
            reply_markup=reply_markup
        )
        return self.SELECT_SSHHOST_ACTION_EXEC


    def select_sshhost(self, update, context):
        msg = "No tengo ganas de trabajar hoy... pero como tengo alternativas, elige un sistema / host:"
        ssh_multi_host = self.configuration.get('ssh_multi_host') or []
        user_data = self.get_user_data_from_config_by_chat_id(self.get_update_username_chat_id(update))
        options = self.filter_options(ssh_multi_host, user_data['ssh_multi_host_access'])
        kwargs = {
            'commandName':'sshhost',
            'msg': msg,
            'options': options
        }
        self.show_options(update, context, **kwargs)
        return self.SELECT_SSHHOST_ACTION

    @_private_function_decorator
    def serverctl(self, update, context):
        msg = "Tenemos estas acciones para el server en el cual estoy alojado y todas apuntan a una muerte segura..."
        self.show_options(update, context, 'serverctl', msg)

    #    Define or execute for this skill method 
        def execute(update, context):
            user_response = self.get_user_keyboard_option_pressed(update)
            if user_response == self.CANCEL_CONV:
                self.cancel_option_query_handler(update, context)
                return False
            self.finish_execute_query_handler()
            skill = importlib.import_module('marvinSkills.private.serverctl')
            result = skill.serverctl(self, update, context, user_response)
            self.send_message(update, context, result['msg'])
            return True

        # Save last and add CallbackQueryHandler for our execution
        self.lastQueryHandler = CallbackQueryHandler(execute)
        self.dispatcher.add_handler(self.lastQueryHandler)
        
    @_private_function_decorator
    def marvinreloadconf(self, update, context):
        msg = "Aquí estoy, con el cerebro del tamaño de un planeta y me pides que recargue el pequeño archivo marvinconf. ¿Llamar a eso satisfacción laboral? Porque no lo disfruto."
        self.send_message(update, context, msg)
        self.configuration.read()
        pass

    @_private_function_decorator
    def bkgsheet(self, update, context):
        msg = "¿ A eso le llamas una hoja de cálculo ?"
        self.show_options(update, context, 'bkgsheet', msg)

    #    Define or execute for this skill method 
        def execute(update, context):
            user_response = self.get_user_keyboard_option_pressed(update)
            if user_response == self.CANCEL_CONV:
                self.cancel_option_query_handler(update, context)
                return False
            self.finish_execute_query_handler()
            skill = importlib.import_module('marvinSkills.private.bkgsheet')
            result = skill.bkgsheet(self, update, context, user_response)
            self.send_message(update, context, result['msg'])
            return True

        # Save last and add CallbackQueryHandler for our execution
        self.lastQueryHandler = CallbackQueryHandler(execute)
        self.dispatcher.add_handler(self.lastQueryHandler)

    @_private_function_decorator
    def fail2banunban(self, update, context):
        msg = "¿ Baneado ?"
        self.send_message(update, context, msg)
        if len(context.args) > 0:
            skill = importlib.import_module('marvinSkills.private.serverctl')
            for ip in context.args:
                self.send_message(update, context, "Desbaneando: "+ip)
                result = skill.fail2banunban(self, update, context, ip)
                self.send_message(update, context, result['msg'])
        else:
            msg = "¿ Esperas que adivine la ip ? Dame una ip seguida del comando: /fail2banunban xxx.xxx.xxx.xxx"
            self.send_message(update, context, msg)

    def yesno(self, update, context):
        import random
        self.send_message(update, context, random.choice(['Yes', 'No']))

    @_private_function_decorator
    def dhltracking(self, update, context):
        msg = "Seguramente otro paquete que no llegará a su destino..."
        self.send_message(update, context, msg)
        if len(context.args) > 0:
            skill = importlib.import_module('marvinSkills.private.dhltracking')
            for tracking_num in context.args:
                self.send_message(update, context, "Número de guía:"+ tracking_num)
                result = skill.dhltracking(self, update, context, tracking_num)
                self.send_message(update, context, result['msg'])
        else:
            msg = "¿ Esperas que adivine el número de guía ? Dame un número de guía seguido del comando: /dhltracking XXXXXXXXX"
            self.send_message(update, context, msg)

    @_private_function_decorator
    def coingeckomonitor(self, update, context):
        msg = "De nuevo vamos con las criptomonedas..."
        self.send_message(update, context, msg)
        if len(context.args) > 4:
            data = {}
            data['ids'] = context.args[0]
            data['vs_currencies'] = context.args[1]
            data['top_value'] = context.args[2]
            data['interval'] = context.args[3]
            data['name'] = context.args[4]
            skill = importlib.import_module('marvinSkills.private.coingecko')
            # self.send_message(update, context, "Monitorearemos la crypto: "+ data['ids'] +" por medio de un hilo hacia la api coingecko, que lanzara un mensaje de aviso en caso de sobrepasar la cantidad "+ data['top_value'] + " en "+ data['vs_currencies'] + " cada " + data['interval'] + " segundos y lo llamaremos "+ data['name'])
            result = skill.coingeckomonitor(self, update, context, data)
            self.send_message(update, context, result['msg'])
        else:
            msg = "Para monitorizar una cripto es necesario pasar los siguientes parametros: ids, vs_currencies, top_value, interval, nombre del monitor \n Ejemplo: /coingeckomonitor <bitcoin|ripple..> <eur|usd...> 4 6 <your_monitor_name_here> \n Más info en https://www.coingecko.com/en/api"
            self.send_message(update, context, msg)

    @_private_function_decorator
    def coingeckomonitorlist(self, update, context):
        skill = importlib.import_module('marvinSkills.private.coingecko') 
        result = skill.coingeckomonitorlist(self, update, context)
        self.send_message(update, context, result['msg'])

    @_private_function_decorator
    def bitsomonitor(self, update, context):
        msg = "De nuevo criptomonedas..."
        self.send_message(update, context, msg)
        if len(context.args) > 3:
            data = {}
            data['ids'] = context.args[0]
            data['top_value'] = context.args[1]
            data['interval'] = context.args[2]
            data['name'] = context.args[3]
            skill = importlib.import_module('marvinSkills.private.bitso')
            result = skill.bitsomonitor(self, update, context, data)
            self.send_message(update, context, result['msg'])
        else:
            msg = "Para monitorizar una cripto es necesario pasar los siguientes parametros: book, top_value, interval, nombre del monitor \n Ejemplo: /bitsomonitor <btc|xrp>_<mxn> 4 6 <your_monitor_name_here> \n Más info en https://bitso.com/api_info#public-rest-api"
            self.send_message(update, context, msg)

    @_private_function_decorator
    def bitsomonitorlist(self, update, context):
        skill = importlib.import_module('marvinSkills.private.bitso') 
        result = skill.bitsomonitorlist(self, update, context)
        self.send_message(update, context, result['msg'])

    def vinchecksum(self, update, context):
        msg = "Otro vehículo a verificar..."
        self.send_message(update, context, msg)
        if len(context.args) > 0:
            skill = importlib.import_module('marvinSkills.public.vinchecksum')
            for vin in context.args:
                result = skill.vinchecksum(self, update, context, vin)
                self.send_message(update, context, result['msg'])
        else:
            msg = "¿ Esperas que adivine el VIN ? Pasame un número de VIN genius... /vinchecksum XXXXXXXXXXXXXXXXX"
            self.send_message(update, context, msg)

def send_post_message(args):
    """Send a direct post message using requests.post('https://api.telegram.org/bot<TOKEN>/sendMessage', data={'chat_id': '<CHAT_ID>', 'text': '<MESSAGE>'})"""
    configuration = Configuration()
    token =  args.token if args.token is not None else configuration.get('token')
    chat_id = None
    if args.to is not None:
        args.to = args.to.lower()
        team_group = configuration.get('team_group')
        if args.to in team_group:
            chat_id =team_group[args.to]['chat_id']
    else:
        chat_id =  args.chat_id if args.chat_id is not None else configuration.get('channel_id')
    message = args.message
    r = requests.post('https://api.telegram.org/bot'+token+'/sendMessage',data={'chat_id': chat_id, 'text': message})
    data = json.loads(r.text)
    if data['ok'] is False:
        log.error("Error sending message, check (-ch 'chat_id'), (-t 'token') or  (-to 'team_group') given by args or defined in "+CONFIG_FILE+', or finally use -h option for help')

if __name__ == '__main__':
    #From globals get logger module
    logging = g.get_logger_module()
    parser = argparse.ArgumentParser(prog=APP, description='Marvin bot options.')
    parser.add_argument('-sm', '--send-message', dest='send_message', action='store_true',help='Send direct message to a channel_id using requests.post()')
    parser.add_argument('-ch', '--chat-id', dest='chat_id', default=None, help='The chat_id, if its not given then use defined in '+CONFIG_FILE)
    parser.add_argument('-t', '--token', dest='token', default=None, help='The bot token, if its not given then use defined in '+CONFIG_FILE)
    parser.add_argument('-m', '--message', dest='message', default=None, help='Message to send')
    parser.add_argument('-to', '--to-team-user', dest='to', default=None, help='To group user')
    parser.add_argument('-l', '--loglevel', dest='loglevel', default=logging.INFO, help='Logging level: INFO, DEBUG, WARNING, CRITICAL, ERROR')
    parser.add_argument('--log-all', help="Log all modules (not only {})".format(APP), action="store_true")

    args = parser.parse_args()

    # Init logging for this module
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=args.loglevel) 
    log = logging.getLogger(APP)

    #Filter logging, if --log-all is not present then only logging this module bot log
    if not args.log_all:
        for handler in logging.root.handlers:
            handler.addFilter(logging.Filter(APP))
    else:
        log.info("Logging all modules.")

    # Set our log in globals 
    g.set_log(log)

    #Send just a direct message ?
    if args.send_message or args.message is not None:
        send_post_message(args)
        exit(0)

    #Run the bot as service background mode.
    bot = MarvinBot()
