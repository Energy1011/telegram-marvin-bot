# Telegram-marvin-bot
![MarvinBot](https://gitlab.com/Energy1011/telegram-marvin-bot/raw/master/marvin.png)

This telegram bot is inpired from atareado.es 'senderbot'.
 I modified the code adding the following:
- Bot Methods/Actions grouping inside a python Class.
- Python decorator to check chat id for 'private' and 'public' method/actions.
- Update code for python-telegram-bot v12 (https://github.com/python-telegram-bot/python-telegram-bot/wiki/Transition-guide-to-Version-12.0)
- Add marvinSkills where you can write your own Methods/Actions. 
- Add an example called 'bkdb' to run a ssh connection to a server and backup a DB using 'private' mode (just for bot's owner).
- New marvinbot.conf structure.
- Add Marvin's personality for this bot from (The Hitchhiker's Guide to the Galaxy)
- marvinbot.service template file for systemd, tested in raspberry pi (Raspbian)
- Add telegram usernames to create internal marvinbot team with team_users in config file

## To run/install your own marvinbot
You have 2 options:
-    1- Installation and run in a docker container (recommended)
-    2- Regular Installation and run in your host machine

## Requirements
First you need 
-    1- Create your bot with botfather and get a token
-    2- Isntall Docker in your system (its optional: if you choose option 1 from above for run marvinbot in a docker container)

# Installation and run in a docker container
This is the recomended way to run Marvinbot (fast and less configurations needed)

### Clon this repo and set your bot token and more in marvinbot configuration file 
```bash
git clone https://gitlab.com/Energy1011/telegram-marvin-bot ~/telegram-marvin-bot
cd ~/telegram-marvin-bot
cp marvinbot.conf.example marvinbot.conf
#IMPORTANT: Edit and add credentials token and private chat id in marvinbot.conf
```

### Building marvinbot docker image
```bash
sudo docker build .
```

### Run Marvinbot: docker container
Option 1: Run marvinbot in interactive mode with auto restart (needed if you want to use the command to restart it)
```bash
sudo docker run --restart always -it --name marvinbot -v $(pwd):/bot marvin-bot:1.0
```
You'll see some output like this:
*2020-11-23 22:08:44,125 - MarvinBot - INFO - MarvinBot Running...*

Once you have runned as above you cand simply run
```
sudo docker start marvinbot
```

*Optional*: run in detach mode
```bash
sudo docker run -d --rm -v $(pwd):/bot --name marvinbot marvin-bot:1.0
```

### Stop Marvinbot: docker container
```bash
sudo docker stop marvinbot
```


# What can we do with Marvinbot?
In config file we can define "marvin skills" to extend it's functionality in private skills and public skills
today in private skills I've programmed the following skills:

- bkdb: Get backups from any remote server you want, accessing it using id_rsa 'public' and 'private' keys using [Paramiko SSH implementation](https://www.paramiko.org/)
- bkgsheet: Backups from google drive spreadsheets using oauth2client and googleapiclient modules
- dhltracing: Using The DHL tracking API
- serverctl: Some function for diagnostic like disk usage, restart, stop bot etc...

In public skills:
 - yesno: simple random 'yes' or 'no' (like toss a coin)
 - fail2banunban: used to unban ips from [fail2ban](https://www.fail2ban.org/wiki/index.php/Main_Page)
 - sendmechatid: echo the chat id between the bot and the user (needed to filter some functions in config file)

### Extra functions: Send messages to a specific team person from command line
>My use case: Imagine a command in which you can create your own group of allowed telegram users to send messages from the terminal using POST from the telegram API, in this way we can for example run a command in the terminal that we do not know when it will end and then send a message to any user of our group to notify that the command has finished atomatically using this command in a short way by a bash alias and using the '&&' operator.
Example with sleep command:
```
sleep 15 && sudo docker run --rm -it -v $(pwd):/bot marvin-bot:1.0 python3 /bot/marvinbot.py -to <username_in_team_users> -m "Wake up! the sleep command has finished!"
```

As described before we can define our group, configure your "team_users" property in your config file *see the marvinbot.conf.example*

Send a message with marvin bot running in docker container mode:
```bash
sudo docker run --rm -it -v $(pwd):/bot marvin-bot:1.0 python3 /bot/marvinbot.py -to <username_in_team_users> -m "<your message here>"
```

The `-to` option will match with your config file team group usernames and chat_ids, remember if your want to know the chat_id you can use the `/sendmechatid` command.



# Logging
For logging it has `--log-all` (if we want to see all modules logging and see what's happening deeply)
```bash
sudo docker run --rm -it --name marvinbot -v $(pwd):/bot marvin-bot:1.0 python3 /bot/marvinbot.py --log-all
```

Additionaly we have  `-l` , `--loglevel` with INFO|DEBUG|WARNING|CRITICAL|ERROR
```bash
sudo docker run --rm -it --name marvinbot -v $(pwd):/bot marvin-bot:1.0 python3 /bot/marvinbot.py --loglevel DEBUG
```

# Install as systemd service and start it (regular installation)
*Initially the project was designed to run as a systemd service without docker, if you wanted to configure the code and adapt it to systemd here you have a template for the service, now I recommend to run it in by docker container better than a systemd service*

## Regular Installation and run in your host machine
```bash
git clone https://gitlab.com/Energy1011/telegram-marvin-bot ~/telegram-marvin-bot
cd ~/telegram-marvin-bot
pip3 install -r requirements.txt
cp marvinbot.conf.example ~/marvinbot.conf
#IMPORTANT: Edit and add credentials token and private chat id in ~/marvinbot.conf
chmod +x ~/telegram-marvin-bot/marvinbot.py
# Start/Run manually
python3 ~/telegram-marvin-bot/marvinbot.py
```

In telegram-marvin-bot folder, open 'marvinbot.service' and edit 'ExecStart' and  'User' if case you need to do it, defaults values are : '~/telegram-marvin-bot/marvinbot.py' and 'pi' raspberrypi user.

then run:
```bash
sudo systemctl enable marvinbot.service
sudo systemctl daemon-reload
sudo systemctl start marvinbot.service
```