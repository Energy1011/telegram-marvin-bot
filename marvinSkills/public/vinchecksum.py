#!/usr/bin/env python3
# This is a marvinSkill to check Vehicle Identification Number (VIN checksum)

MODULE_NAME = 'vinchecksum'

def vinchecksum(inst, update, context, vin):
    transliteration = {
        'A':1, 'B':2, 'C':3, 'D':4, 'E':5, 'F':6, 'G':7, 'H':8,
        'J':1, 'K':2, 'L':3, 'M':4, 'N':5, 'P':7, 'R':9,
        'S':2, 'T':3, 'U':4, 'V':5, 'W':6, 'X':7, 'Y':8, 'Z':9,
    }
    weights = (8,7,6,5,4,3,2,10,0,9,8,7,6,5,4,3,2)
    vin = vin.upper()
    length = len(vin)
    vinsum = 0
    if length != 17 or vin == '11111111111111111':
        result = {'status': False, 'msg':'Checksum: Error vin check lenght, must be 17 chars'}
        return result

    for x in range(0,length):
        char = vin[x]
        if char.isnumeric():
            vinsum = vinsum + (int(char) * int(weights[x]))
        else:
            if char not in transliteration.keys():
                result = {'status': False, 'msg':'Checksum: Invalid char: '+char+' in-> '+vin}
                return result
            vinsum = vinsum + (transliteration[char] * weights[x])

    remainder = vinsum % 11;
    if remainder == 10:
        checkdigit = 'X'
    else:
        checkdigit = remainder


    if str(vin[8]) != str(checkdigit):
        result = {'status': False, 'msg':'Checksum: Not valid-> '+vin}
    else:
        result = {'status': True, 'msg':'Checksum: OK-> '+vin}
    return result