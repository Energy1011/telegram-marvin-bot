#!/usr/bin/env python3
# This is a marvinSkill for bitso monitor
# More info:  https://bitso.com/api_info#public-rest-api
import threading
import time
import queue
import requests

MODULE_NAME = 'bitso'
ENDPOINT_FORMAT= 'bitsoApi'

# emojis
EMJ_ROLLING_EYES = u'\U0001F644'

threads = []

class MyTheread(threading.Thread):

    def __init__(self, data):
        self.__ids = data['ids']
        self.__top_value = data['top_value']
        self.__interval = int(data['interval'])
        self.__name = data['name']
        self.__inst = data['inst']
        self.__update = data['update']
        self.__context = data['context']
        threading.Thread.__init__(self)

    def getPrice(self):
        url = "https://api.bitso.com/v3/ticker/?book="+self.getIds()
        response = requests.get(url, params={}, 
        headers= {'accept':'application/json'})
        jsonCheck = response.json()
        price = jsonCheck['payload']['last']
        return price

    def run(self):
        while(True):
            price  = self.getPrice()
            if float(price) >= float(self.getTopValue()):
                self.__inst.send_message(
                    self.__update, self.__context, EMJ_ROLLING_EYES +" Puede que le importe que el top value definido del monitor ha sido alcanzado \n" + self.getResume())
                break
            time.sleep(self.__interval)

    def getTopValue(self):
        return self.__top_value

    def getIds(self):
        return self.__ids

    def getName(self):
        return self.__name

    def getResume(self):
        return "Monitor: '"+ self.__name+"'  de la cripto "+ self.__ids + " notificar si el valor es mayor a $" +  str(self.__top_value) + "  cada " + str(self.__interval) + " segundos.\n"

def bitsomonitorlist(inst, update, context):
    removeNotAliveThreads()
    monitors_resume = ""
    if len(threads) > 0 :
        for thread in threads:
            monitors_resume =  monitors_resume + thread.getResume() + "\n"
        return {'msg':  "Listado de monitores de precios actualmente trabajando:\n" + monitors_resume}
    else:
        return {'msg':  "No tenemos monitores de precios actualmente trabajando.\n"}

def removeNotAliveThreads():
    for i, t in enumerate(threads):
        if not t.is_alive():
            del threads[i]

def bitsomonitor(inst, update, context, data):
    data['inst'] = inst
    data['update'] = update
    data['context'] = context
    #TODO check chat_id to send notification
    worker = MyTheread(data)
    worker.start()
    threads.append(worker)
    removeNotAliveThreads()
    return { 'msg':"El monitor de precio para la cripto se ha creado." }
