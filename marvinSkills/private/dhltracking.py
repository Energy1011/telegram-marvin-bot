#!/usr/bin/env python3
# This is a marvinSkill for dhl tracking

import requests
import json

MODULE_NAME = 'dhltracking'
ENDPOINT_FORMAT= 'dhlapi'

def dhltracking(inst, update, context, tracking_num):
    response = requests.get('https://api-eu.dhl.com/track/shipments', params={'trackingNumber': tracking_num}, 
    headers= {'DHL-API-Key':inst.configuration.get('marvinSkills')[MODULE_NAME]['dhl_customer_key']})
    jsonCheck = response.json()
    if 'shipments' in jsonCheck:
        myjson = str(jsonCheck['shipments']).replace("\'", "\"")
        response = json.loads(myjson)
        response = response[0]
        result = {'status': True , 'msg':'DHL response, ok status: '+str(response['status'])}
    else:
        result = {'status': False, 'msg':'DHL response: '+response.text}
    return result