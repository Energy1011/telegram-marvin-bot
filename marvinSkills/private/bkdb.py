#!/usr/bin/env python3
# This is a marvinSkill example to do a database backup via ssh connection

import paramiko
from paramiko import SSHClient
from scp import SCPClient
import os
from g import g #Globals

MODULE_NAME = 'bkdb'
ENDPOINT_FORMAT= 'sh'

def sshCommand(inst, update, context, cmd):
    client = createSSHClient(inst.configuration.get('ssh_server'),
        inst.configuration.get('ssh_user'),
        inst.configuration.get('ssh_port'),
        inst.configuration.get('ssh_key_file_password'),
        inst.configuration.get('ssh_keyfile_path'))
    stdin, stdout, stderr = client.exec_command(cmd)
    stdout = ''.join(stdout.readlines())
    inst.send_message(update, context, MODULE_NAME+"."+ENDPOINT_FORMAT+" output: " + stdout)
    client.close()
    return stdin, stdout, stderr

def createSSHClient(server, user, port, password, keyfile):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.load_system_host_keys()
    client.connect(server, username=user, port=port, password=password, key_filename=keyfile)
    return client

def bkdb(inst, update, context, database):
    log = g.get_log()
    msg = 'Ejecutando end point: '+MODULE_NAME+"."+ENDPOINT_FORMAT
    inst.send_message(update, context, msg)
    # Check for ssh_keyfile_path
    if os.path.exists(inst.configuration.get('ssh_keyfile_path')):
        client = createSSHClient(inst.configuration.get('ssh_server'),
            inst.configuration.get('ssh_user'),
            inst.configuration.get('ssh_port'),
            inst.configuration.get('ssh_key_file_password'),
            inst.configuration.get('ssh_keyfile_path'))
    else:
        log.critical('Path key file error, check your ssh_keyfile_path.')
        result = {'status': False, 'msg': 'Key file path error', 'error': 'Error.'}
        return result
    
    cmd = ENDPOINT_FORMAT+' '+inst.configuration.get('marvin_end_point_dir')+MODULE_NAME+'.'+ENDPOINT_FORMAT+' '+database
    stdin, stdout, stderr = sshCommand(inst, update, context, cmd)

    scp = SCPClient(client.get_transport())
    filename = inst.configuration.get('marvin_dir')+MODULE_NAME+os.sep+'last-db_'+database+'.sql'
    scp.get(inst.configuration.get('marvin_end_point_dir')+MODULE_NAME+os.sep+'last-db_'+database+'.sql', inst.configuration.get('marvin_dir')+MODULE_NAME+os.sep+'last-db_'+database+'.sql')
    client.close()
    #send document to private channel_id
    if os.path.exists(filename):
        inst.sendDocument(chat_id=inst.channel_id,document=open(filename,'rb'),timeout=999)
    try:
        stdout = stdout.readlines()
    except:
        pass 
    result = {'status': True, 'msg':stdout, 'error':stderr}
    return result