#!/usr/bin/env python3
# This is a marvinSkill for coingecko monitor
# More info en https: // www.coingecko.com/en/api
import threading
import time
import queue
import requests

MODULE_NAME = 'coingecko'
ENDPOINT_FORMAT= 'coingeckoApi'

# emojis
EMJ_ROLLING_EYES = u'\U0001F644'

threads = []

class MyTheread(threading.Thread):

    def __init__(self, data):
        self.__ids = data['ids']
        self.__vs_currencies = data['vs_currencies']
        self.__top_value = data['top_value']
        self.__interval = int(data['interval'])
        self.__name = data['name']
        self.__inst = data['inst']
        self.__update = data['update']
        self.__context = data['context']
        threading.Thread.__init__(self)

    def getPrice(self):
        url = "https://api.coingecko.com/api/v3/simple/price?ids="+self.getIds()+"&vs_currencies="+self.getVsCurrencies()
        response = requests.get(url, params={}, 
        headers= {'accept':'application/json'})
        jsonCheck = response.json()
        if self.getIds() in jsonCheck:
            price = jsonCheck[self.getIds()][self.getVsCurrencies()]
            return price
        else:
            return -1

    def run(self):
        while(True):
            price  = self.getPrice()
            if float(price) >= float(self.getTopValue()):
                self.__inst.send_message(
                    self.__update, self.__context, u'\U0001F644'+" Puede que le importe que el top value definido del monitor ha sido alcanzado \n" + self.getResume())
                break
            time.sleep(self.__interval)

    def getTopValue(self):
        return self.__top_value

    def getVsCurrencies(self):
        return self.__vs_currencies

    def getIds(self):
        return self.__ids

    def getName(self):
        return self.__name

    def getResume(self):
        return "Monitor: '"+ self.__name+"'  de la cripto "+ self.__ids + " en " + self.__vs_currencies + " notificar si el valor es mayor a $" +  str(self.__top_value) + "  cada " + str(self.__interval) + " segundos.\n"

def coingeckomonitorlist(inst, update, context):
    removeNotAliveThreads()
    monitors_resume = ""
    if len(threads) > 0 :
        for thread in threads:
            monitors_resume =  monitors_resume + thread.getResume() + "\n"
        return {'msg':  "Listado de monitores de precios actualmente trabajando:\n" + monitors_resume}
    else:
        return {'msg':  "No tenemos monitores de precios actualmente trabajando.\n"}

def removeNotAliveThreads():
    for i, t in enumerate(threads):
        if not t.is_alive():
            del threads[i]

def coingeckomonitor(inst, update, context, data):
    data['inst'] = inst
    data['update'] = update
    data['context'] = context
    #TODO check chat_id to send notification
    worker = MyTheread(data)
    worker.start()
    threads.append(worker)
    removeNotAliveThreads()
    return { 'msg':"El monitor de precio para la cripto se ha creado." }