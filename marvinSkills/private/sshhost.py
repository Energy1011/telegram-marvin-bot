#!/usr/bin/env python3
# This is a marvinSkill example to do a database backup via ssh connection

import paramiko
from paramiko import SSHClient
from scp import SCPClient
import os
from g import g #Globals

MODULE_NAME = ''
ENDPOINT_FORMAT= ''

def sshCommand(inst, update, context, cmd):
    client = createSSHClient(inst.configuration.get('ssh_server'),
        inst.configuration.get('ssh_user'),
        inst.configuration.get('ssh_port'),
        inst.configuration.get('ssh_key_file_password'),
        inst.configuration.get('ssh_keyfile_path'))
    stdin, stdout, stderr = client.exec_command(cmd)
    stdout = ''.join(stdout.readlines())
    inst.send_message(update, context,"Output: " + stdout)
    client.close()
    return stdin, stdout, stderr

def createSSHClient(server, user, port, password, keyfile):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.load_system_host_keys()
    client.connect(server, username=user, port=port, password=password, key_filename=keyfile)
    return client

def sshhost(inst, update, context, host, action):
    log = g.get_log()
    msg = 'Ejecutando endpoint para host ('+host+')'
    inst.send_message(update, context, msg)

    h_config = inst.configuration.get('ssh_multi_host')[host] or None
    # Check for host config
    if h_config:
        client = createSSHClient(h_config['ssh_server'],
            h_config['ssh_user'],
            h_config['ssh_port'],
            inst.configuration.get('ssh_key_file_password'),
            inst.configuration.get('ssh_keyfile_path'))
    else:
        log.critical('Check for your keyfile or config.')
        result = {'status': False, 'msg': 'Key file path or config', 'error': 'Error.'}
        return result
    
    cmd = 'bash '+inst.configuration.get('marvin_end_point_dir')+action+'.'+'sh '+host
    stdin, stdout, stderr = sshCommand(inst, update, context, cmd)
    client.close()

    #send document to private channel_id
    result = {'status': True, 'msg':stdout, 'error':stderr}
    return result