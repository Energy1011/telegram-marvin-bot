#!/usr/bin/env python3
# This is a marvinSkill to control bot server by custom commands
import os

MODULE_NAME = 'serverctl'
ENDPOINT_FORMAT= 'sh'

def serverctl(inst, update, context, cmd):
    if cmd == 'container-bot:restart':
        # if we are running marvinbot in a docker container with (--restart always) flag, we can only terminate script by os._exit(0) and wait for docker container --restart
        inst.send_message(update, context, "Reiniciando... El verdadero horror de la existencia de Marvin es que ninguna tarea podría ocupar ni la más mínima fracción de su vasto intelecto.")
        os._exit(0)
    result = os.popen(cmd).read()
    if not result:
        result = cmd
    result = {'status': True, 'msg':'Output: '+result}
    return result

def fail2banunban(inst, update, context, ip):
    cmd = 'sudo fail2ban-client set sshd unbanip '+ip
    result = os.popen(cmd).read()
    if not result:
        result = cmd
    result = {'status': True, 'msg':'Fail2ban: '+result}
    return result