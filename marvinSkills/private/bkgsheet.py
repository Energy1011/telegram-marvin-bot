#!/usr/bin/env python3
from oauth2client import file, client, tools
from googleapiclient.discovery import build
from googleapiclient.http import MediaIoBaseDownload
from httplib2 import Http

import io
import datetime
import os
from g import g

MODULE_NAME = 'bkgsheet'
ENDPOINT_FORMAT= 'googleapiclient'

SCOPES = 'https://www.googleapis.com/auth/drive'

def bkgsheet(inst, update, context, sheet_to_download):
    log = g.get_log()
    file_id,  file_prefix = sheet_to_download.split(' ')
    tokenpath = inst.configuration.get('marvin_conf_dir')+MODULE_NAME+os.sep+'token.json'
    credentialspath = inst.configuration.get('marvin_conf_dir')+MODULE_NAME+os.sep+'credentials.json'
    if not os.path.exists(credentialspath) or not os.path.exists(credentialspath):
        log.error("Incorrect path {} or {}".format(credentialspath, tokenpath))
        return  {'status': False, 'msg':"Ocurrio un error, verifique su archivo de configuración"}
    store = file.Storage(tokenpath)
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets(credentialspath, SCOPES)
        creds = tools.run_flow(flow, store)
    drive_service = build('drive', 'v3', http=creds.authorize(Http()))
    request = drive_service.files().export_media(
        fileId=file_id, mimeType='application/vnd.oasis.opendocument.spreadsheet')
    fh = io.BytesIO()
    downloader = MediaIoBaseDownload(fh, request)
    done = False
    msg = 'Ejecutando end point: '+MODULE_NAME+"."+ENDPOINT_FORMAT
    inst.send_message(update, context, msg)
    while done is False:
        status, done = downloader.next_chunk()
    md = datetime.datetime.now()
    filename = file_prefix+'_'+str(md.month)+"_"+str(md.day)+"_"+str(md.year)+"_"+str(md.hour)+"_"+str(md.minute)+".xlsx"
    filename = inst.configuration.get('marvin_dir')+MODULE_NAME+os.sep+filename
    with open(filename, 'wb') as out:
        out.write(fh.getvalue())
    #send document to private channel_id
    if os.path.exists(filename):
        inst.sendDocument(chat_id=inst.channel_id,document=open(filename,'rb'),timeout=999)
    return  {'status': True, 'msg':"Se ha realizado la descarga de respaldo"}
